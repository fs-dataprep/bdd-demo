# Behavior Driven Development Demo Project #

## New Content

Updated for Cucumber 6, Hamcrest 2, and JUnit 4.  I don't have staged tags build in yet but you can use the master
branch as a baseline for understanding the project setup and poking at unit tests with Cucumber.

### Useful Links
* [Cucumber.io](http://cucumber.io/docs/cucumber) for more detailed documentation.
* [Mockito](https://site.mockito.org/) for using mocks and spys in testing.
* [Hamcrest](http://hamcrest.org/JavaHamcrest/index) for assertions and matchers.

## Old Content

This project is a simple Maven based JavaFX RPN stack calculator 
intended to function as the code companion to the [Introduction
to Behavior Driven Development](https://teams.ldschurch.org/sites/Pre-PublicationOperationsTeamSite/_layouts/PowerPoint.aspx?PowerPointView=ReadingView&PresentationId=/sites/Pre-PublicationOperationsTeamSite/Shared%20Documents/Training/BDD/Behavior%20Driven%20Development%20Intro.pptx&Source=https%3a//teams.ldschurch.org/sites/Pre-PublicationOperationsTeamSite/Shared%2520Documents/Forms/AllItems.aspx?RootFolder%3D%252Fsites%252FPre%252DPublicationOperationsTeamSite%252FShared%2520Documents%252FTraining%252FBDD) powerpoint presentation.

Various tags in this repository correspond to steps outlined
in the slides.

### Tags for JUnit Version (using Java 7 syntax) ###

Tag   | Development Phase
---   | -----------------
Step1 | Basic project with sample test harness
Step2 | Feature specification for the Addition function
Step3 | Step definitions for the Addition feature
Step4 | Implementation of the Addition function for insufficient operands
Step5 | Implementation of the Addition function to actually add numbers

### Tags for TestNG Version (using Java 8 syntax) ###
Checkout the TestNG branch prior to grabbing the individual tags.

Tag    | Development Phase
------ | -----------------
TNG-01 | Basic project with test harness and placeholder files for the Feature specification and Test Steps
TNG-02 | First feature scenario: Hello World for Addition
TNG-03 | Implementing the first set of test steps
TNG-04 | Implementing the add function to satisfy the first test scenario
TNG-05 | Expanding the scenario into a data-driven test
TNG-06 | Additional test step implementation to support the new scenario outline
TNG-07 | Additional code to support the new scenario outline
TNG-08 | Failure cases
TNG-09 | Failure case step implementation
TNG-10 | Failure case code implementation
