package org.familysearch.ops.bdddemo;

import javafx.application.Application;
import static javafx.application.Application.launch;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * The base class for the BDD Demo FXML application.
 *
 * @author Andrew Wheelwright <andrew.wheelwright@familysearch.org>
 */
public class App extends Application {
    private static Logger LOGGER = LogManager.getLogManager().getLogger(Application.class.getName());

    /**
     * The main entry point for all JavaFX applications. The start method is
     * called after the {@link Application#init} method has returned, and after the system is ready
     * for the application to begin running.
     *
     * <p>
     * NOTE: This method is called on the JavaFX Application Thread.
     * </p>
     *
     * @param stage The primary stage for this application, onto which the
     * application scene can be set. The primary stage will be embedded in the
     * browser if the application was launched as an applet. Applications may
     * create other stages, if needed, but they will not be primary stages and
     * will not be embedded in the browser.
     * @throws java.lang.Exception When bad things happen.
     */
    @Override
    public void start(Stage stage) throws Exception {
        Parent root;
        Scene scene;

        Platform.setImplicitExit(true);
        try {
            root = FXMLLoader.load(getClass().getResource("/org/familysearch/ops/bdddemo/views/App.fxml"));
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Could not load the application view.", ex);
            System.exit(-2);
            return;
        }

        scene = new Scene(root);
        scene.getStylesheets().add("/org/familysearch/ops/bdddemo/styles/App.css");
        stage.setTitle("BDD Demo App : " + getClass().getPackage().getImplementationVersion());

        stage.setScene(scene);
        stage.initStyle(StageStyle.UTILITY);
        stage.setResizable(true);
        stage.centerOnScreen();
        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
