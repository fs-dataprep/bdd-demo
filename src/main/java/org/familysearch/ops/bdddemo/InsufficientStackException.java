package org.familysearch.ops.bdddemo;

/**
 * Signals that the user attempted an operation that required more
 * elements than the Stack contains.
 *
 * @author Andrew Wheelwright <andrew.wheelwright@familysearch.org>
 */
public class InsufficientStackException extends IllegalStateException {
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates an InsufficientStackException with no detail message.
     */
    public InsufficientStackException() {
    }

    /**
     * Instantiates an InsufficientStackException with the given detail message.
     *
     * @param string The detail message.
     */
    public InsufficientStackException(String string) {
        super(string);
    }

    /**
     * Instantiates an InsufficientStackException wrapping the specified cause.
     *
     * @param cause The cause of the exception.
     */
    public InsufficientStackException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new InsufficientStackException with the specified detail message
     * and cause.
     *
     * @param string The detail message.
     * @param cause The cause of the exception.
     */
    public InsufficientStackException(String string, Throwable cause) {
        super(string, cause);
    }
}
