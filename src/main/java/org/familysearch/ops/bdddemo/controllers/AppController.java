package org.familysearch.ops.bdddemo.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.familysearch.ops.bdddemo.services.Calculator;

public class AppController implements Initializable {
    /**
     * The calculator managed by this controller.
     */
    private final Calculator calculator;

    /**
     * The result stack.
     */
    @FXML
    private ListView<Double> resultListView;

    /**
     * The button used to concatenate '0' to the scratch register.
     */
    @FXML
    private Button digit0Button;

    /**
     * The button used to concatenate a decimal point '.' to the scratch
     * register.
     */
    @FXML
    private Button decimalButton;

    /**
     * The button used to concatenate '1' to the scratch register.
     */
    @FXML
    private Button digit1Button;

    /**
     * The button used to concatenate '2' to the scratch register.
     */
    @FXML
    private Button digit2Button;

    /**
     * The button used to concatenate '3' to the scratch register.
     */
    @FXML
    private Button digit3Button;

    /**
     * The button used to concatenate '4' to the scratch register.
     */
    @FXML
    private Button digit4Button;

    /**
     * The button used to concatenate '5' to the scratch register.
     */
    @FXML
    private Button digit5Button;

    /**
     * The button used to concatenate '6' to the scratch register.
     */
    @FXML
    private Button digit6Button;

    /**
     * The button used to concatenate '7' to the scratch register.
     */
    @FXML
    private Button digit7Button;

    /**
     * The button used to concatenate '8' to the scratch register.
     */
    @FXML
    private Button digit8Button;

    /**
     * The button used to concatenate '9' to the scratch register.
     */
    @FXML
    private Button digit9Button;

    /**
     * The addition button.
     */
    @FXML
    private Button addButton;

    /**
     * The subtraction button.
     */
    @FXML
    private Button subtractButton;

    /**
     * The multiplication button.
     */
    @FXML
    private Button multiplyButton;

    /**
     * The division button.
     */
    @FXML
    private Button divideButton;

    /**
     * The data entry button.
     */
    @FXML
    private Button enterButton;

    /**
     * The button pressed to remove characters from the scratch register.
     */
    @FXML
    private Button backspaceButton;

    /**
     * The button pressed to drop stack entries.
     */
    @FXML
    private Button dropButton;

    /**
     * The button pressed to swap the top two stack entries.
     */
    @FXML
    private Button swapButton;

    /**
     * The button pressed to clear the scratch register or stack area.
     */
    @FXML
    private Button clearButton;

    /**
     * The scratch register.
     */
    @FXML
    private TextField scratchTextField;

    /**
     * Initializes the controllers local variables.
     */
    public AppController() {
        this.calculator = new Calculator();
    }

    /**
     * Initializes the calculator controller.
     *
     * @param location The location used to resolve relative paths for the root
     * object, or
     * <tt>null</tt> if the location is not known.
     *
     * @param resources The resources used to localize the root object, or
     * <tt>null</tt> if the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resultListView.setItems(null);
        this.scratchTextField.textProperty().bind(this.calculator.getScratchProperty().asString());
    }

    /**
     * Handles the digit 0 button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDigit0(ActionEvent event) {
        this.calculator.handleKeyPress(KeyCode.DIGIT0);
    }

    /**
     * Handles the decimal button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDecimalPoint(ActionEvent event) {
        this.calculator.handleKeyPress(KeyCode.DECIMAL);
    }

    /**
     * Handles the digit 1 button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDigit1(ActionEvent event) {
        this.calculator.handleKeyPress(KeyCode.DIGIT1);
    }

    /**
     * Handles the digit 2 button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDigit2(ActionEvent event) {
        this.calculator.handleKeyPress(KeyCode.DIGIT2);
    }

    /**
     * Handles the digit 3 button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDigit3(ActionEvent event) {
        this.calculator.handleKeyPress(KeyCode.DIGIT3);
    }

    /**
     * Handles the digit 4 button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDigit4(ActionEvent event) {
        this.calculator.handleKeyPress(KeyCode.DIGIT4);
    }

    /**
     * Handles the digit 5 button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDigit5(ActionEvent event) {
        this.calculator.handleKeyPress(KeyCode.DIGIT5);
    }

    /**
     * Handles the digit 6 button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDigit6(ActionEvent event) {
        this.calculator.handleKeyPress(KeyCode.DIGIT6);
    }

    /**
     * Handles the digit 7 button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDigit7(ActionEvent event) {
        this.calculator.handleKeyPress(KeyCode.DIGIT7);
    }

    /**
     * Handles the digit 8 button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDigit8(ActionEvent event) {
        this.calculator.handleKeyPress(KeyCode.DIGIT8);
    }

    /**
     * Handles the digit 9 button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDigit9(ActionEvent event) {
        this.calculator.handleKeyPress(KeyCode.DIGIT9);
    }

    /**
     * Handles the enter button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onEnter(ActionEvent event) {
        this.calculator.enter();
    }

    /**
     * Handles the addition button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onAdd(ActionEvent event) {
        this.calculator.add();
    }

    /**
     * Handles the subtraction button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onSubtract(ActionEvent event) {
        this.calculator.subtract();
    }

    /**
     * Handles the multiplication button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onMultiply(ActionEvent event) {
        this.calculator.multiply();
    }

    /**
     * Handles the division button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDivide(ActionEvent event) {
        this.calculator.divide();
    }

    /**
     * Handles the backspace button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onBackspace(ActionEvent event) {
        this.calculator.backspace();
    }

    /**
     * Handles the drop button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onDrop(ActionEvent event) {
        this.calculator.drop();
    }

    /**
     * Handles the swap button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onSwap(ActionEvent event) {
        this.calculator.swap();
    }

    /**
     * Handles the clear button action.
     *
     * @param event The event data.
     */
    @FXML
    private void onClear(ActionEvent event) {
        this.calculator.clear();
    }

    /**
     * Handles key type events supported by the application.
     *
     * @param event The event data.
     */
    @FXML
    private void onKeyTyped(KeyEvent event) {
        this.calculator.handleKeyPress(event.getCode());
    }
}
