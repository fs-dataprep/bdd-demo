package org.familysearch.ops.bdddemo.services;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.scene.input.KeyCode;
import org.familysearch.ops.bdddemo.InsufficientStackException;

/**
 * A simple reverse polish notation stackProperty calculator with basic math
 operations (add, subtract, multiply, and divide) as well as some
 rudimentary stackProperty operations (enter, drop, swap, clear).
 *
 * @author Andrew Wheelwright <andrew.wheelwright@familysearch.org>
 */
public class Calculator {
    /**
     * The result stack property.
     */
    private final ListProperty<Double> stackProperty;

    /**
     * The scratch space used for entering numbers into the calculator.
     */
    private final ObjectProperty<Double> scratchProperty;

    /**
     * Instantiates a reverse polish notation stackProperty calculator with an empty
 stackProperty.
     */
    public Calculator() {
        this.stackProperty = new SimpleListProperty<>(FXCollections.observableArrayList());
        this.scratchProperty = new SimpleObjectProperty<>();
    }

    /**
     * Adds the specified digit or decimal point to the scratchProperty register.
     * @param key The key designating the digit or decimal place.
     */
    public void handleKeyPress(KeyCode key) {
        // TODO: implement the key handler
    }

    /**
     * Pops two numbers off the stackProperty, adds them together, then pushes the result on the stackProperty.
     * @throws InsufficientStackException If the stackProperty does not contain at least two entries.
     */
    public void add() throws InsufficientStackException {
        /*
         * Stack (a list under the covers)
         * -----
         * 1: 5.4
         * 2: 3.8
         *
         * Add translates to 5.4 + 3.8 = 9.2
         */

        this.stackProperty.add(pop() + pop());
    }

    /**
     * Pops two numbers off the stackProperty, subtracts the first number from the second, then pushes the result on the stackProperty.
     * @throws InsufficientStackException If the stackProperty does not contain at least two entries.
     */
    public void subtract() throws InsufficientStackException {
        Double right = pop();
        Double left = pop();
        this.stackProperty.add(left - right);
    }

    /**
     * Pops two numbers off the stackProperty, multiplies them together, then pushes the result on the stackProperty.
     * @throws InsufficientStackException If the stackProperty does not contain at least two entries.
     */
    public void multiply() throws InsufficientStackException {
        // TODO: implement the multiply method
    }

    /**
     * Pops two numbers off the stackProperty, divides the second number by the first, then pushes the result on the stackProperty.
     * @throws InsufficientStackException If the stackProperty does not contain at least two entries.
     */
    public void divide() throws InsufficientStackException {
        // TODO: implement the add method
    }

    /**
     * If there is a value in the scratchProperty register, adds the value to the stackProperty; otherwise, duplicates the value on the top of the stackProperty.
     */
    public void enter() {
        // TODO: implement the enter method
    }

    /**
     * Removes the last digit/character from the scratchProperty register.
     */
    public void backspace() {
        // TODO: implement the backspace method
    }

    /**
     * If the scratchProperty space is not empty, clears it; otherwise, removes all
 items from the stackProperty.
     */
    public void clear() {
        // TODO: implement the clear method
    }

    /**
     * Exchanges the top two items on the stackProperty.
     * @throws InsufficientStackException If the stackProperty does not contain at least two
 entries.
     */
    public void swap() throws InsufficientStackException {
        // TODO: implement the swap method
    }

    /**
     * Removes the top-most item from the stackProperty.
     * @throws InsufficientStackException If the stackProperty does not contain at least one
 entries.
     */
    public void drop() throws InsufficientStackException {
        // TODO: implement the drop method
    }

    /**
     * Gets the stack property.
     * @return The stack property object.
     */
    public ListProperty<Double> getStackProperty() {
        return stackProperty;
    }

    /**
     * Gets the scratch property.
     * @return The scratch register property object.
     */
    public ObjectProperty<Double> getScratchProperty() {
        return scratchProperty;
    }

    /**
     * Pops the top item off the stack.
     *
     * @return The top item.
     * @throws InsufficientStackException if the stackProperty does not contain at least one entry.
     */
    private Double pop() {
        if (this.stackProperty.isEmpty()) {
            throw new InsufficientStackException("Cannot pop an empty stack.");
        }

        return this.stackProperty.remove(this.stackProperty.size() - 1);
    }
}
