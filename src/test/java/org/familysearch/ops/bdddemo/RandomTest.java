package org.familysearch.ops.bdddemo;

import org.familysearch.ops.bdddemo.services.Calculator;
import org.junit.Test;

import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Random JUnit test example.
 *
 * @author Andrew Wheelwright (andrew.wheelwright@familysearch.org)
 */
public class RandomTest {
    /**
     * Simple test to show how JUnit works.
     */
    @Test
    public void testRandomNumbers() {
        Random random = new Random();
        int upperBound = 11;
        int actual = random.nextInt(upperBound);

        assertThat(actual, is(both(greaterThanOrEqualTo(0)).and(lessThan(upperBound))));
    }

    /**
     * Verifies that subtracting with insufficient items on the stack
     * produces an exception.
     */
    @Test(expected = InsufficientStackException.class)
    public void testSubtractingWithInsufficentOperands() {
        Calculator calculator = new Calculator();
        calculator.getStackProperty().add(1D);
        assertThat(calculator.getStackProperty(), hasSize(1));
        assertThat(calculator.getStackProperty().get(0), is(equalTo(1D)));
        calculator.subtract();
    }
}
