package org.familysearch.ops.bdddemo;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
import org.junit.runner.RunWith;

/**
 * This class allows JUnit to run BDD test cases described using Cucumber
 * feature files.
 *
 * @author Andrew Wheelwright <andrew.wheelwright@familysearch.org>
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {
                "pretty",
                "html:target/test-report.html",
                "junit:target/test-report.xml"
        },
        snippets = SnippetType.CAMELCASE
)
public class TestRunner {
    // no content needed
}
