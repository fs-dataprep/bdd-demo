package org.familysearch.ops.bdddemo.services;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.familysearch.ops.bdddemo.InsufficientStackException;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Implements the feature test steps for the {@link Calculator} class.
 *
 * @author Andrew Wheelwright <andrew.wheelwright@familysearch.org>
 */
public class CalculatorSteps {
    /**
     * The amount of allowable difference when comparing double values.
     * The IEEE standard for floating point numbers is inherently inaccurate
     * which requires a loose comparison with a margin of error.
     */
    public static final double DOUBLE_COMPARISON_ERROR_MARGIN = 0.000001;

    /**
     * The class under test.
     */
    private final Calculator calculator;

    /**
     * The error thrown by the test method.
     */
    private Throwable error;

    /**
     * Random number generator.
     */
    private final Random generator;

    /**
     * Randomly generated input values used to be used for comparing with results.
     */
    private List<Double> randomInputs;

    /**
     * Dependency injection constructor.  PicoContainer will create the calculator class
     * at run time using the default constructor for that class.
     *
     * @param calculator The calculator to test.
     */
    public CalculatorSteps(Calculator calculator) {
        this.calculator = calculator;
        this.error = null;
        this.generator = new Random();
    }

    @Given("a stack with the numbers: {}")
    public void aStackWithTheNumbers(String inputStrings) throws Throwable {
        List<Double> inputs = convertStringsToDoubles(inputStrings);
        resetStack();
        calculator.getStackProperty().addAll(inputs);
        verifyStack(calculator.getStackProperty(), inputs);
    }

    @Given("a stack with {long} number(s)")
    public void aStackWithNumbers(Long stackSize) throws Throwable {
        randomInputs = generator.doubles(stackSize)
                .boxed()
                .collect(Collectors.toList());

        resetStack();
        calculator.getStackProperty().addAll(randomInputs);
        verifyStack(calculator.getStackProperty(), randomInputs);
    }

    @When("Calculator::add is called")
    public void calculatorAddIsCalled() throws Throwable {
        try {
            calculator.add();
        } catch (Throwable error) {
            this.error = error;
        }
    }

    @When("Calculator::subtract is called")
    public void calculatorSubtractIsCalled() {
        try {
            calculator.subtract();
        } catch (Throwable error) {
            this.error = error;
        }
    }

    @Then("no exception should be thrown")
    public void noExceptionShouldBeThrown() throws Throwable {
        assertThat(this.error, is(nullValue(Throwable.class)));
    }

    @Then("^the stack should contain the number[s]?: (.*)$")
    public void theStackShouldContain(String expectedValueStrings) throws Throwable {
        List<Double> expectedValues = convertStringsToDoubles(expectedValueStrings);
        verifyStack(this.calculator.getStackProperty(), expectedValues);
    }

    @Then("the value on the stack should equal the sum of the original numbers")
    public void theValueOnTheStackShouldEqualTheSumOfTheOriginalNumbers() throws Throwable {
        verifyStack(
                this.calculator.getStackProperty(),
                Arrays.asList(randomInputs.stream()
                        .mapToDouble(Double::doubleValue)
                        .sum())
        );
    }

    @Then("an exception should be thrown: InsufficientStackException")
    public void anExceptionShouldBeThrownInsufficientStackException() {
        assertThat(this.error, is(instanceOf(InsufficientStackException.class)));
    }

    /**
     * Converts a value containing comma-delimited numerics into a list of doubles.
     *
     * @param inputStrings The input to parse.
     * @return The list of doubles.
     */
    private List<Double> convertStringsToDoubles(String inputStrings) {
        return Stream.of(inputStrings.split(",\\s*"))
                .mapToDouble(Double::parseDouble)
                .boxed()
                .collect(Collectors.toList());
    }

    /**
     * Verifies that the list of actual doubles is reasonably close to the
     * expected list of doubles.
     *
     * @param actual The actual list.
     * @param expected The expected list.
     * @throws Throwable If the lists do not match.
     */
    private void verifyStack(List<Double> actual, List<Double> expected) throws Throwable {
        assertThat(actual.size(), is(equalTo(expected.size())));
        for (int index = 0, maxIndex = expected.size(); index < maxIndex; ++index) {
            assertThat(actual.get(index), is(closeTo(expected.get(index), DOUBLE_COMPARISON_ERROR_MARGIN)));
        }
    }

    /**
     * Ensures the {@link Calculator} is initialized to with an empty stack.
     * @throws Throwable if the Calculator has not been instantiated or the stack is not empty after initialization.
     */
    private void resetStack() throws Throwable {
        assertThat(
                "The calculator should be instantiated via dependency injection but wasn't.",
                this.calculator,
                is(notNullValue(Calculator.class))
        );

        this.calculator.getStackProperty().clear();
        assertThat(
                "The calculator should have an empty stack after being cleared.",
                this.calculator.getStackProperty(),
                is(empty())
        );
    }
}
