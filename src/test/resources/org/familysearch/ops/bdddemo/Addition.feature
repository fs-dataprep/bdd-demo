Feature: Addition Operator

    RPN calculators use a stack for performing operations.  Numbers
    are pushed on the stack and manipulated with math operators (e.g. add,
    subtract, multiply, and divide) and stack operators (e.g. drop, swap,
    and clear).

    Scenario: Adding numbers
        Given a stack with 2 numbers
        When Calculator::add is called
        Then no exception should be thrown
        And the value on the stack should equal the sum of the original numbers

    Scenario Outline: Adding numbers
        Given a stack with the numbers: <input>
        When Calculator::add is called
        Then no exception should be thrown
        And the stack should contain the numbers: <output>
        Examples:
            | input     | output |
            | 1, 2      | 3      |
            | 2, 4, 5   | 2, 9   |
            | 3.45, 2.6 | 6.05   |
            | -5.3, 2.1 | -3.2   |

    Scenario: Adding with insufficient operands
        Given a stack with 1 number
        When Calculator::add is called
        Then an exception should be thrown: InsufficientStackException

