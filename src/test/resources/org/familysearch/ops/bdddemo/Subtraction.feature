# Created by drewed at 26May2021
Feature: Subtraction Operator
  RPN calculators use a stack for performing operations.  Numbers
  are pushed on the stack and manipulated with math operators (e.g. add,
  subtract, multiply, and divide) and stack operators (e.g. drop, swap,
  and clear).

  Scenario Outline: Subtracting with two numbers
    Given a stack with the numbers: <input>
    When Calculator::subtract is called
    Then no exception should be thrown
    And the stack should contain the numbers: <output>
    Examples:
      | input     | output |
      | 1, 2      | -1     |
      | 2, 1      | 1      |
      | 2, 9, 5   | 2, 4   |
      | 3.45, 2.6 | 0.85   |
      | -5.3, 2.1 | -7.4   |

  Scenario: Subtracting with insufficient operands
    Given a stack with 1 number
    When Calculator::subtract is called
    Then an exception should be thrown: InsufficientStackException
